import {StackNavigator} from "react-navigation";
import SignInScreen from "../screens/SignInScreen";
import CheckSmsCodeScreen from "../screens/CheckSmsCodeScreen";
import {pinkDarkColor} from "../config/styleConstants";

const headerProps = {
    headerStyle: {
        backgroundColor: pinkDarkColor,
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
        fontWeight: 'bold',
    }
};

export default StackNavigator({
    SignIn: {
        screen: SignInScreen,
        navigationOptions: {
            title: 'Sign in'
        }
    },
    CheckSMS: {
        screen: CheckSmsCodeScreen,
        navigationOptions: {
            title: 'Sign in'
        }
    },
}, {
    navigationOptions: {
        ...headerProps
    }
});
