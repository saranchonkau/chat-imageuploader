import ImagesScreen from "../screens/ImagesScreen";
import {pinkDarkColor} from "../config/styleConstants";
import {StackNavigator} from "react-navigation";
import SignOutButton from "../components/SignOutButton";
import React from "react";
import ChatListScreen from '../screens/ChatListScreen';
import MainMenuScreen from '../screens/MainMenuScreen';
import MessagesScreen from "../screens/MessagesScreen";

const headerProps = {
    headerStyle: {
        backgroundColor: pinkDarkColor,
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
        fontWeight: 'bold',
    }
};

export default StackNavigator({
    MainMenu: {
        screen: MainMenuScreen,
        navigationOptions: ({ navigation }) => ({
            title: 'Main menu',
            headerRight: <SignOutButton navigation={navigation}/>
        })
    },
    Images: {
        screen: ImagesScreen,
        navigationOptions: ({ navigation }) => ({
            title: 'Images',
            headerRight: <SignOutButton navigation={navigation}/>
        })
    },
    ChatList: {
        screen: ChatListScreen,
        navigationOptions: ({ navigation }) => ({
            title: 'Chat list',
            headerRight: <SignOutButton navigation={navigation}/>
        })
    },
    Messages: {
        screen: MessagesScreen,
        navigationOptions: ({ navigation }) => ({
            headerRight: <SignOutButton navigation={navigation}/>
        })
    },
}, {
    navigationOptions: {
        ...headerProps
    }
});
