import AuthLoadingScreen from "../screens/AuthLoadingScreen";
import {SwitchNavigator} from "react-navigation";
import AppStack from './AppNavigation';
import AuthStack from './AuthNavigation';

export default SwitchNavigator(
    {
        AuthLoading: AuthLoadingScreen,
        App: AppStack,
        Auth: AuthStack
    }
);

export const Screens = {
    SignIn: 'SignIn',
    MainMenu: 'MainMenu',
    Images: 'Images',
    ChatList: 'ChatList',
    Chat: 'Chat'
};
