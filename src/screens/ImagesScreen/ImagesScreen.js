import React, {Component} from 'react';
import {View, StyleSheet, ActivityIndicator, FlatList} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import {pinkColor, pinkDarkColor} from "../../config/styleConstants";
import {Image} from "../../utils/utils";
import ImageItem from '../../components/ImageItem';
import Button from '../../components/Button';
import {uniqBy} from 'lodash';
import {sendImages} from "../../utils/requests";
import {connect} from "react-redux";
import {addImage, clearAll, updateProgress} from "./ImageScreenActionCreators";

const ImagePickerOptions = {
    title: 'Select image',
    takePhotoButtonTitle: 'Take a photo',
    chooseFromLibraryButtonTitle: 'Choose from gallery',
    quality: 1,
    storageOptions: {
        skipBackup: true,
        path: 'images'
    }
};

class Images extends Component {

    constructor(){
        super();
        this.state = {
            isLoading: false
        }
    }

    selectImage = () => {

        this.setState({ isLoading: true });

        ImagePicker.showImagePicker(ImagePickerOptions, (response) => {
            console.log('Picked image : ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
                this.setState({ isLoading: false });
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
                this.setState({ isLoading: false });
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                this.setState({ isLoading: false });
            } else {
                this.props.loadImage(new Image(response));
                this.setState({ isLoading: false });
            }
        });
    };

    sendImages = () => {
        console.log('Send Images...');
        this.setState({ isLoading: true });
        sendImages(this.props.images, this.props.updateUploadProgress)
            .then(() => {
                this.props.removeAll();
                this.setState({ isLoading: false});
            })
            .catch(console.log);
    };

    render(){
        const { removeAll, images  } = this.props;
        const { isLoading } = this.state;
        const isExist = images.length > 0;
        const buttonContainerStyles = isExist ?
            StyleSheet.flatten([styles.buttonContainer, { justifyContent: 'space-between' }]) :
            styles.buttonContainer;
        return (
            <View style={styles.container}>
                {
                    isExist &&
                    <FlatList data={images}
                              style={styles.listItem}
                              keyExtractor={ item => item.uri }
                              renderItem={({item}) => <ImageItem item={item}/>}
                    />
                }
                { isLoading && <ActivityIndicator style={styles.activityIndicator} size='large' color={'white'}/> }
                <View style={buttonContainerStyles}>
                    <Button onPress={this.selectImage} text={'Add image'}/>
                    { isExist && <Button onPress={this.sendImages} text={'Submit'}/> }
                    { isExist && <Button onPress={removeAll} text={'Clear all'}/> }
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: pinkColor,
    },
    buttonContainer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingHorizontal: 20,
        paddingVertical: 10
    },
    image: {
        width: 150,
        height: 150
    },
    listItem: {
        marginBottom: 20,
        width: '100%',
        backgroundColor: pinkColor,
        borderColor: pinkDarkColor,
        borderBottomColor: pinkDarkColor
    },
    activityIndicator: {
        position: 'absolute',
        zIndex: 200,
        height: 100,
        width: 100
    }
});

export default connect(
    state => ({
        images: state.images.images
    }),
    dispatch => ({
        removeAll: () => dispatch(clearAll()),
        loadImage: image => dispatch(addImage(image)),
        updateUploadProgress: (imageIndex, progress) => dispatch(updateProgress(imageIndex, progress))
    })
)(Images);