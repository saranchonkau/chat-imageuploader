import {uniqBy} from 'lodash';

const initialState = {
    images: [],
    lastIndex: 0
};

const addImage = (state, image) => {
    const newIndex = state.lastIndex + 1;
    image.setIndex(newIndex);
    console.log('Added image: ', image);
    return { images: uniqBy([...state.images, image], 'uri'), lastIndex: newIndex };
};

const updateProgress = (state, imageIndex, progress) => {
    const images = [];
    state.images.reduce((previous, current) => {
        if (current.index === imageIndex) {
            current.setProgress(progress);
        }
        images.push(current);
    }, images);

    return { ...state, images };
};

const imagesReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'IMAGE_ADD': return addImage(state, action.image);
        case 'IMAGE_REMOVE': return { ...state, images: state.images.filter(image => image.uri !== action.image.uri) };
        case 'PROGRESS_UPDATE': return updateProgress(state, action.imageIndex, action.progress);
        case 'CLEAR_ALL': return { ...state, images: [] };
        default: return state;
    }
};

export default imagesReducer;