export const addImage = image => ({
    type: 'IMAGE_ADD',
    image
});

export const removeImage = image => ({
    type: 'IMAGE_REMOVE',
    image
});

export const clearAll = () => ({
    type: 'CLEAR_ALL'
});

export const updateProgress = (imageIndex, progress) => {
    console.log(`Imahe index = ${imageIndex},  progress = ${progress}`);
    return {
    type: 'PROGRESS_UPDATE',
    imageIndex,
    progress
}};