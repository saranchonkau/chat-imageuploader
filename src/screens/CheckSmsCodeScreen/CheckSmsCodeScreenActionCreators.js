export const updateSmsCode = code => ({
    type: 'CODE_SMS_UPDATE',
    code
});
export const updateSmsCodeError = error => ({
    type: 'CODE_SMS_ERROR_UPDATE',
    error
});