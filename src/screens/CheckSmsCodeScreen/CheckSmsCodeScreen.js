import React, { Component } from 'react';
import {
    Text,
    View
} from 'react-native';
import Logo from '../../components/Logo';
import {saveToken} from "../../utils/utils";
import FormInput from "../../components/FormInput";
import Button from "../../components/Button";
import {styles} from "./CheckSmsCodeScreenStyles";
import {connect} from 'react-redux';
import {updateSmsCode, updateSmsCodeError} from "./CheckSmsCodeScreenActionCreators";
import {clearSignInState} from "../SignInScreen/SignInScreenActionCreators";
import {requestToken} from "../../utils/requests";

class CheckSmsCodeScreen extends Component {

    componentDidMount(){
        this.codeInput.focus();
    }

    onSubmit = () => {
        const {navigation, updateError, clearState, number, code} = this.props;
        requestToken(number, code)
            .then(response => {
                console.log('Response: ', response);
                saveToken(response.token);
                this.blurCodeInput();
                clearState();
                navigation.navigate('MainMenu');
            })
            .catch(({ errors }) => {
                console.log('Errors: ', errors);
                if (errors && errors.code && errors.code.length > 0) {
                    updateError(errors.code.map(error => error.message).join('. '));
                } else {
                    updateError('Unknown error. Please contact your administrator');
                }
            });
        };

    setCodeInput = input => {
        this.codeInput = input;
    };

    blurCodeInput = () => this.codeInput.isFocused() && this.codeInput.blur();

    render() {
        const { updateCode, code, codeError, correctCode } = this.props;
        return (
            <View style={styles.container}>
                <Logo/>
                <Text style={styles.welcome}>
                    Welcome to BLAKIT!
                </Text>
                <View>
                    <FormInput placeholder={`SMS code (${correctCode})`}
                               keyboardType='phone-pad'
                               onChangeText={updateCode}
                               value={code}
                               error={codeError}
                               inputRef={this.setCodeInput}
                    />
                    <Button text={'SUBMIT'} onPress={this.onSubmit}/>
                </View>
            </View>
        );
    }
}


export default connect(
    state => ({
        code: state.signIn.codeSms,
        codeError: state.signIn.codeSmsError,
        correctCode: state.signIn.correctSmsCode,
        number: state.signIn.phoneNumber
    }),
    dispatch => ({
        updateCode: code => dispatch(updateSmsCode(code)),
        updateError: error => dispatch(updateSmsCodeError(error)),
        clearState: () => dispatch(clearSignInState())
    })
)(CheckSmsCodeScreen);
