import React, {Component} from 'react';
import {GiftedChat} from 'react-native-gifted-chat';
import {StyleSheet, View} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {actionCreators as chatActionCreators, stateSelectors as chatStateSelectors} from "./MessagesScreenReducer";
import {actionCreators as socketActionCreators} from "../../sagas/chatWebsocketSaga";
import {stateSelectors as profileStateSelectors} from "../MainMenuScreen/MainMenuScreenReducer";
import uuid from 'uuid/v4';
import Icon from 'react-native-vector-icons/FontAwesome';
import {styles} from "./MessagesScreenStyles";

class ChatScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        const { params } = navigation.state;
        return {
            title: params ? params.opponentName : 'Messages',
        }
    };

    constructor(){
        super();
        this.state = {
            messageText: ''
        };
    }

    componentWillUnmount(){
        this.props.updateCurrentRoom('');
    }

    componentDidMount(){
        const { chat: { id }} = this.props;
        console.tron.log(this.props);
        this.props.fetchMessages({ chatId: id, messageId: null, count: 10 });
        this.props.readChat({ room: this.props.currentRoom });
    }

    componentWillReceiveProps(nextProps) {
        const lastMessageId = this.props.messages.length && this.props.messages[0].id;
        const newLastMessageID = nextProps.messages.length && nextProps.messages[0].id;
        if (newLastMessageID && lastMessageId !== newLastMessageID) {
            this.props.readChat({ room: this.props.currentRoom });
        }
    }

    convertMessages = () => {
        return this.props.messages.map(({ id, type, content, timestamp, user }) => {
            return {
            _id: id,
            text: type === 'TEXT' && content,
            createdAt: new Date(timestamp * 1000),
            user: {
                _id: user.id,
                name: user.name,
                avatar: user.avatar
            },
            image: type === 'IMAGE' && content,
            sent: true,
            received: timestamp <= this.props.opponentReadTime
        }})
    };

    onMessageTextChange = messageText => this.setState({ messageText });

    sendMessage = () => {
        const message = {
            guid: uuid(),
            room: this.props.currentRoom,
            timezone: '+03:00',
            type: 'TEXT',
            content: this.state.messageText
        };
        this.props.sendMessage(message);
        this.setState({ messageText: '' });
    };

    renderCustomActions = props => {
        return (
            <View style={StyleSheet.flatten([ styles.attachButton, { height: props.composerHeight } ])}>
                <Icon name="paperclip"
                      backgroundColor={'transparent'}
                      underlayColor={'gray'}
                      size={25}
                />
            </View>
        );
    };

    loadEarlierMessages = () => {
        const { chat: { id }, messages, fetchMoreMessages } = this.props;
        fetchMoreMessages({ chatId: id, messageId: messages[messages.length - 1].id, count: 10 });
    };

    render(){
        const { hasMore, userId } = this.props;
        return (
            <GiftedChat
                text={this.state.messageText}
                onInputTextChanged={this.onMessageTextChange}
                messages={this.convertMessages()}
                placeholder={'Ваше сообщение...'}
                user={{ _id: userId }}
                renderActions={this.renderCustomActions}
                onSend={this.sendMessage}
                loadEarlier={hasMore}
                onLoadEarlier={this.loadEarlierMessages}
                keyboardShouldPersistTaps={'never'}
            />
        );
    }
}

export default connect(
    state => ({
        messages: chatStateSelectors.getMessages(state),
        chat: chatStateSelectors.getCurrentChatInfo(state),
        userId: profileStateSelectors.getUserId(state),
        opponentReadTime: chatStateSelectors.getOpponentReadTime(state),
        hasMore: chatStateSelectors.hasMoreMessages(state),
        currentRoom: chatStateSelectors.getCurrentRoom(state)
    }),
    dispatch => ({
        ...bindActionCreators(
            {
                ...chatActionCreators,
                ...socketActionCreators
            },
            dispatch
        )
    })
)(ChatScreen);