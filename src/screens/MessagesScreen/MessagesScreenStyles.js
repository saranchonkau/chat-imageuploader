import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    attachButton: {
        justifyContent: 'center',
        paddingLeft: 20,
        paddingRight: 10,
        marginRight: 0
    }
});
