import {actionTypes as socketActionTypes} from "../../sagas/chatWebsocketSaga";

/*#######   Action types   #######*/

export const actionTypes = {
    FETCH_MESSAGES: 'FETCH_MESSAGES',
    FETCH_MESSAGES_SUCCESS: 'FETCH_MESSAGES_SUCCESS',

    FETCH_MORE_MESSAGES: 'FETCH_MORE_MESSAGES',
    FETCH_MORE_MESSAGES_SUCCESS: 'FETCH_MORE_MESSAGES_SUCCESS',

    UPDATE_CURRENT_ROOM: 'UPDATE_CURRENT_ROOM'
};

/*#######   Initial state   #######*/

const initialState = {
    list: [],
    hasMore: false,
    isLoading: false,
    currentRoom: ''
};

/*#######   Reducer   #######*/

export default (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_MESSAGES_SUCCESS: return {...state, list: action.list, hasMore: action.has_more};
        case actionTypes.FETCH_MORE_MESSAGES_SUCCESS: return {...state, list: [...state.list, ...action.list], hasMore: action.has_more};
        case actionTypes.UPDATE_CURRENT_ROOM: return {...state, currentRoom: action.room};
        case socketActionTypes.NEW_MESSAGE: return {...state, list: [action.message, ...state.list]};
        default: return state;
    }
}

/*#######   Action creators   #######*/

export const actionCreators = {
    fetchMessages: payload => ({ type: actionTypes.FETCH_MESSAGES, payload }),
    fetchMessagesSuccess: ({ list, has_more }) => ({ type: actionTypes.FETCH_MESSAGES_SUCCESS, list, has_more }),
    fetchMoreMessages: payload => ({ type: actionTypes.FETCH_MORE_MESSAGES, payload }),
    fetchMoreMessagesSuccess: ({ list, has_more }) => ({ type: actionTypes.FETCH_MORE_MESSAGES_SUCCESS, list, has_more }),
    updateCurrentRoom: room => ({ type: actionTypes.UPDATE_CURRENT_ROOM, room })
};

/*#######   State selectors   #######*/

export const stateSelectors = {
    getMessages: state => state.messages.list,
    getCurrentChatInfo: state => {
        const currentChat = state.chatList.list.find(chat => chat.room === state.messages.currentRoom );
        return currentChat || {};
    },
    getOpponentReadTime: state => {
        const currentChat = state.chatList.list.find(chat => chat.room === state.messages.currentRoom );
        return currentChat ? currentChat.opponent_read_at : '';
    },
    hasMoreMessages: state => state.messages.hasMore,
    getCurrentRoom: state => state.messages.currentRoom
};