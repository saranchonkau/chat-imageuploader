import {pinkColor, pinkDarkColor} from "../../config/styleConstants";
import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: pinkColor,
    },
    listItem: {
        marginBottom: 20,
        width: '100%',
        backgroundColor: pinkColor,
        borderColor: pinkDarkColor,
        borderBottomColor: pinkDarkColor
    },
    errorText: {
        color: 'white',
        fontSize: 16,
        marginVertical: 30
    }
});
