import {actionTypes as socketActionTypes} from "../../sagas/chatWebsocketSaga";

/*#######   Action types   #######*/

export const actionTypes = {
    FETCH_CHAT_LIST: 'FETCH_CHAT_LIST',
    FETCH_CHAT_LIST_SUCCESS: 'FETCH_CHAT_LIST_SUCCESS',
    FETCH_CHAT_LIST_FAIL: 'FETCH_CHAT_LIST_FAIL',
};

/*#######   Initial state   #######*/

const initialState = {
    list: [],
    isLoading: false,
    error: ''
};

/*#######   Helper functions   #######*/

const addNewMessage = (state, message) => {
    const updatedChatIndex = state.list.findIndex(chat => chat.room === message.room);
    if (updatedChatIndex !== -1) {
        const updatedChat = {
            ...state.list[updatedChatIndex],
            has_unread: true,
            unread_count: Number.parseInt(state.list[updatedChatIndex].unread_count) + 1,
            last_message: message
        };
        const updatedList = [
            updatedChat,
            ...state.list.slice(0, updatedChatIndex),
            ...state.list.slice(updatedChatIndex + 1)
        ];
        return { ...state, list: updatedList };
    }
    return state;
};

const updateOpponentReadTime = (state, response) => {
    const updatedChatIndex = state.list.findIndex(chat => chat.room === response.room);
    const updatedList = state.list.map((chat, index) => {
        // opponent read chat
        if (index === updatedChatIndex) {
            if (chat.opponent && chat.opponent.id === response.user_id) {
                return { ...chat, opponent_read_at: response.timestamp };
            } else {
                // user read chat
                return { ...chat, unread_count: '0', has_unread: false };
            }
        } else {
            return chat;
        }
    });
    return { ...state, list: updatedList };
};

/*#######   Reducer   #######*/

export default (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_CHAT_LIST: return { ...state, isLoading: true };
        case actionTypes.FETCH_CHAT_LIST_SUCCESS: return { list: action.list, isLoading: false, error: '' };
        case actionTypes.FETCH_CHAT_LIST_FAIL: return { list: [], isLoading: false, error: 'Some error was happened. Please update page' };
        case socketActionTypes.NEW_MESSAGE: return addNewMessage(state, action.message);
        case socketActionTypes.CHAT_READ_SUCCESS: return updateOpponentReadTime(state, action.response);

        default: return state;
    }
};

/*#######   Action creators   #######*/

export const actionCreators = {
    fetchChatList: () => ({ type: actionTypes.FETCH_CHAT_LIST }),
    fetchChatListSuccess: list => ({ type: actionTypes.FETCH_CHAT_LIST_SUCCESS, list }),
    fetchChatListFail: () => ({ type: actionTypes.FETCH_CHAT_LIST_FAIL })
};

/*#######   State selectors   #######*/

export const stateSelectors = {
    getChatList: state => state.chatList.list,
    getChatListLoadingStatus: state => state.chatList.isLoading,
    getChatListFetchingError: state => state.chatList.error
};