import React, {Component} from 'react';
import {View, FlatList, RefreshControl, Text} from 'react-native';
import ChatItem from "../../components/ChatItem/ChatItem";
import {styles} from "./ChatListScreenStyles";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {actionCreators, stateSelectors} from "./ChatListScreenReducer";
import {actionCreators as socketActionCreators} from "../../sagas/chatWebsocketSaga";
import {pinkDarkColor} from "../../config/styleConstants";

class ChatListScreen extends Component {

    componentDidMount(){
        this.props.fetchChatList();
        this.props.connectChatSocket();
    }

    componentWillUnmount(){
        console.tron.log('UNMOUNT');
        this.props.closeChatSocket();
    }

    renderItem = ({ item }) =>
        <ChatItem chat={item}
                  onPress={() => this.props.navigation.navigate('Messages', {
                      opponentName: item.opponent ? item.opponent.name : null
                  })}
        />;

    getKeyExtractor = chat => chat.id.toString();

    render(){
        const { chatList, isLoading, error } = this.props;
        return (
            <View style={styles.container}>
                {
                    chatList.length > 0 &&
                    <FlatList data={chatList}
                              style={styles.listItem}
                              keyExtractor={this.getKeyExtractor}
                              renderItem={this.renderItem}
                              refreshControl={
                                  <RefreshControl
                                      onRefresh={() => this.props.fetchChatList()}
                                      refreshing={isLoading}
                                      colors={[pinkDarkColor]}
                                  />
                              }
                    />
                }
                {
                    error ?
                    <Text style={styles.errorText}>{error}</Text> : false
                }
            </View>
        );
    }
}

export default connect(
    state => ({
        chatList: stateSelectors.getChatList(state),
        isLoading: stateSelectors.getChatListLoadingStatus(state),
        error: stateSelectors.getChatListFetchingError(state)
    }),
    dispatch => ({
        ...bindActionCreators(
            {...actionCreators, ...socketActionCreators},
            dispatch)
    })
)(ChatListScreen);