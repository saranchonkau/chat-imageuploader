import React from 'react';
import {
    ActivityIndicator,
    AsyncStorage,
    StatusBar,
    StyleSheet,
    View,
} from 'react-native';
import {pinkColor} from "../config/styleConstants";

class AuthLoadingScreen extends React.Component {
    constructor(props) {
        super(props);
        console.log('Start token searching...');
        this.getAuthToken();
    }

    getAuthToken = () =>  {
        return AsyncStorage.getItem('userToken')
            .then(token => {
                console.log('Token', token);
                console.log(token ? 'Token was found!' : 'Token not found!');
                this.props.navigation.navigate(token ? 'App' : 'Auth');
            })
            .catch(error => console.log('Some error was thrown while token was searching', error));
    };

    render() {
        return (
            <View style={styles.container}>
                <ActivityIndicator size='large' color={'white'}/>
                <StatusBar barStyle="default" />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: pinkColor,
    }
});

export default AuthLoadingScreen;