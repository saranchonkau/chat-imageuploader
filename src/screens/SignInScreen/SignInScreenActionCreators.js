export const updatePhoneNumber = number => ({
    type: 'PHONE_NUMBER_UPDATE',
    number
});

export const updatePhoneNumberError = error => ({
    type: 'PHONE_NUMBER_ERROR_UPDATE',
    error
});

export const saveCorrectSmsCode = code => ({
    type: 'CORRECT_SMS_CODE_SAVE',
    code
});

export const clearSignInState = () => ({
    type: 'STATE_CLEAR'
});