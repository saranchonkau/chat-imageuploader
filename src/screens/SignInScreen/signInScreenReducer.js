const initialState = {
    phoneNumber: '',
    phoneNumberError: '',
    codeSms: '',
    codeSmsError: '',
    correctSmsCode: ''
};

const signInReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'PHONE_NUMBER_UPDATE': return {...state, phoneNumber: action.number};
        case 'PHONE_NUMBER_ERROR_UPDATE': return {...state, phoneNumberError: action.error};
        case 'CORRECT_SMS_CODE_SAVE': return {...state, correctSmsCode: action.code};
        case 'CODE_SMS_UPDATE': return {...state, codeSms: action.code};
        case 'CODE_SMS_ERROR_UPDATE': return {...state, codeSmsError: action.error};
        case 'STATE_CLEAR': return {...initialState};
        default: return state;
    }
};

export default signInReducer;