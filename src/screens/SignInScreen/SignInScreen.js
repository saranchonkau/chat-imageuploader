import React, { Component } from 'react';
import {Text, View} from 'react-native';
import Logo from '../../components/Logo';
import FormInput from "../../components/FormInput";
import Button from "../../components/Button";
import {styles} from "./SignInScreenStyles";
import {connect} from 'react-redux';
import {saveCorrectSmsCode, updatePhoneNumber, updatePhoneNumberError} from "./SignInScreenActionCreators";
import {requestCode} from "../../utils/requests";

class SignIn extends Component {

    componentDidMount(){
        this.phoneInput.focus();
    }

    onSubmit = () => {
        const {navigation, updateError, saveCode} = this.props;
        if (this.validate()) {
            requestCode(this.props.number)
                .then(response => {
                    console.log('Response: ', response);
                    saveCode(response.code);
                    this.blurPhoneInput();
                    updateError('');
                    navigation.navigate('CheckSMS');
                })
                .catch(({ errors }) => {
                    if (errors && errors.phone && errors.phone.length > 0) {
                        updateError(errors.phone.map(error => error.message).join('. '));
                    } else {
                        updateError('Unknown error. Please contact your administrator');
                    }
                });
        }
    };

    blurPhoneInput = () => this.phoneInput.isFocused() && this.phoneInput.blur();

    setPhoneInput = input => {
        this.phoneInput = input;
    };

    validate = () => {
        const phonePattern = /^\+[0-9]{12}$/;
        const {number, updateError} = this.props;

        if (!number.trim()) {
            updateError('Required');
        } else if (!phonePattern.test(number)) {
            updateError('Wrong format !');
        } else {
            return true;
        }

        return false;
    };

    render() {
        const { updateNumber, number, numberError } = this.props;
        return (
            <View style={styles.container}>
                <Logo/>
                <Text style={styles.welcome}>
                    Welcome to BLAKIT!
                </Text>
                <View>
                    <FormInput placeholder='Phone (e.g. +375336240202)'
                               keyboardType='phone-pad'
                               onChangeText={updateNumber}
                               value={number}
                               error={numberError}
                               inputRef={this.setPhoneInput}
                    />
                    <Button text={'SUBMIT'} onPress={this.onSubmit}/>
                </View>
            </View>
        );
    }
}


export default connect(
    state => ({
        number: state.signIn.phoneNumber,
        numberError: state.signIn.phoneNumberError
    }),
    dispatch => ({
        updateNumber: number => dispatch(updatePhoneNumber(number)),
        updateError: error => dispatch(updatePhoneNumberError(error)),
        saveCode: code => dispatch(saveCorrectSmsCode(code))
    })
)(SignIn);
