import {StyleSheet} from "react-native";
import {pinkColor} from "../../config/styleConstants";

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: pinkColor,
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        color: 'white',
        marginBottom: 60
    },
    instructions: {
        textAlign: 'center',
        color: '#ffffff',
        marginBottom: 5,
    },
});
