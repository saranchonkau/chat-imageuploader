import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import {pinkColor} from "../../config/styleConstants";
import Button from "../../components/Button";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {actionCreators} from "./MainMenuScreenReducer";

class MainMenuScreen extends Component {

    componentDidMount(){
        this.props.fetchProfile();
    }

    render(){
        const { navigation } = this.props;
        return (
            <View style={styles.container}>
                <Button text={'Go to Images'} onPress={() => navigation.navigate('Images')}/>
                <Button text={'Go to Chats'} onPress={() => navigation.navigate('ChatList')}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: pinkColor,
    },
});

export default connect(
    null,
    dispatch => ({
        ...bindActionCreators(actionCreators, dispatch)
    })
)(MainMenuScreen);