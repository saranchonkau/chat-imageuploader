/*#######   Action types   #######*/

export const actionTypes = {
    FETCH_PROFILE: 'FETCH_PROFILE',
    FETCH_PROFILE_SUCCESS: 'FETCH_PROFILE_SUCCESS'
};

/*#######   Initial state   #######*/

const initialState = {
    user: {}
};

/*#######   Reducer   #######*/

export default (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_PROFILE_SUCCESS: return {user: action.profile};
        default: return state;
    }
};

/*#######   Action creators   #######*/

export const actionCreators = {
    fetchProfile: () => ({ type: actionTypes.FETCH_PROFILE }),
    fetchProfileSuccess: profile => ({ type: actionTypes.FETCH_PROFILE_SUCCESS, profile }),
};

/*#######   State selectors   #######*/

export const stateSelectors = {
    getProfile: state => state.profile,
    getUserId: state => state.profile.user.id
};