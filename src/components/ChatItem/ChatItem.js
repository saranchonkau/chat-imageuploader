import React, {Component} from 'react';
import {pinkDarkColor} from "../../config/styleConstants";
import {ListItem} from "react-native-elements";
import moment from 'moment';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {actionCreators} from "../../screens/MessagesScreen/MessagesScreenReducer";
import ChatItemRight from '../ChatItemRight';
import {stateSelectors as profileStateSelectors} from "../../screens/MainMenuScreen/MainMenuScreenReducer";
import defaultLogo from '../../images/user-circle.svg';

class ChatItem extends Component {

    getLastMessage = () => {
        const { last_message } = this.props.chat;
        if (last_message) {
            switch (last_message.type) {
                case 'TEXT': return last_message.content;
                case 'IMAGE': return 'Изображение';
                default: return '';
            }
        }
        return '';
    };

    getLastMessageTime = () => {
        const { last_message } = this.props.chat;
        return last_message ? moment(last_message.timestamp * 1000).format('hh:mm') : '';
    };

    onPress = () => {
        this.props.updateCurrentRoom(this.props.chat.room);
        this.props.onPress();
    };

    getSubtitleStyle = () => {
        const { chat: { last_message, opponent_read_at }, userId} = this.props;
        const subtitleStyle = {
            flex: 1,
            fontSize: 16,
            color: 'black',
            borderRadius: 5,
            paddingHorizontal: 5
        };
        // if this is my message and opponent haven't read it yet
        if ((last_message && last_message.user.id === userId) && (opponent_read_at && opponent_read_at < last_message.timestamp)) {
            return { ...subtitleStyle,  backgroundColor: pinkDarkColor };
        } else {
            return subtitleStyle;
        }
    };

    getContainerStyle = () => {
        const containerStyle = { borderBottomColor: pinkDarkColor };
        return this.props.chat.has_unread ?
            { ...containerStyle,  backgroundColor: pinkDarkColor } :
            containerStyle;
    };

    getChatAvatar = () => {
        const { opponent } = this.props.chat;
        if (opponent && opponent.avatar) {
            return { uri: opponent.avatar.url };
        }
        return { source: defaultLogo };
    };

    render(){
        console.tron.log(this.props);
        const { chat: { opponent, unread_count, title }} = this.props;
        return (
            <ListItem
                roundAvatar
                avatar={this.getChatAvatar()}
                title={opponent ? opponent.name : title}
                titleNumberOfLines={2}
                subtitle={this.getLastMessage()}
                hideChevron={true}
                avatarStyle={{height: 50, width: 50}}
                avatarContainerStyle={{height: 50, width: 50}}
                titleStyle={{color: 'white', fontSize: 18}}
                subtitleStyle={this.getSubtitleStyle()}
                containerStyle={this.getContainerStyle()}
                badge={{ element: <ChatItemRight unreadCount={unread_count} time={this.getLastMessageTime()}/> }}
                onPress={this.onPress}
            />
        );
    }
}

export default connect(
    state => ({
        userId: profileStateSelectors.getUserId(state)
    }),
    dispatch => ({ ...bindActionCreators(actionCreators, dispatch) })
)(ChatItem);