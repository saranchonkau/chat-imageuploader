import React, { Component } from 'react';
import {View} from 'react-native';
import FormInput from './FormInput';
import Button from "./Button";

class LoginForm extends Component {

    constructor(){
        super();
        this.state = {
            email: '',
            emailError: '',
            password: '',
            passwordError: ''
        };

        this.emailPattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    }

    handleChange = fieldName => text => this.setState({ [fieldName]: text });

    getPasswordInput = passwordInputRef => this.passwordInput = passwordInputRef;

    submitForm = () => {
        const { email, password } = this.state;
        if (this.validate()) {
            this.props.onSubmit({email, password});
        }
    };

    validate = () => {
        const {email, password} = this.state;

        if (!email.trim()) {
            this.setState({ emailError: 'Required', passwordError: '' });

        } else if (!this.emailPattern.test(email.trim().toLowerCase())) {
            this.setState({ emailError: 'Email have wrong format', passwordError: '' });

        } else if (!password) {
            this.setState({ emailError: '', passwordError: 'Required' });

        } else {
            return true;
        }
        return false;
    };

    render(){
        const { email, emailError, password, passwordError } = this.state;
        return (
            <View>
                <FormInput placeholder='Email'
                           keyboardType='email-address'
                           onSubmitEditing={() => this.passwordInput.focus()}
                           onChangeText={this.handleChange('email')}
                           value={email}
                           error={emailError}
                />
                <FormInput placeholder='Password'
                           secureTextEntry={true}
                           selectTextOnFocus={true}
                           inputRef={this.getPasswordInput}
                           onChangeText={this.handleChange('password')}
                           value={password}
                           error={passwordError}
                />
                <Button onPress={this.submitForm} text={'LOGIN'}/>
            </View>
        );
    }
}

export default LoginForm;