import React from 'react';
import {Text, View, StyleSheet} from "react-native";

const ChatItemRight = ({ time, unreadCount }) => (
    <View style={styles.container}>
        {
            unreadCount > 0 &&
            <View style={styles.unreadCountContainer}>
                <Text style={styles.unreadCountText}>{unreadCount}</Text>
            </View>
        }
            <View>
                <Text>{time}</Text>
            </View>
    </View>
);

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        backgroundColor: 'transparent',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginLeft: 10
    },
    unreadCountContainer: {
        backgroundColor: '#2E37FF',
        borderRadius: 20,
        paddingVertical: 5,
        paddingHorizontal: 10,
        marginRight: 10
    },
    unreadCountText: {
        color: 'white'
    }
});

export default ChatItemRight;