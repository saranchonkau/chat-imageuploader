import {signOut} from "../../utils/requests";
import {pinkLightColor} from "../../config/styleConstants";
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {connect} from 'react-redux';
import {logOutUser} from "./SignOutButtonActionCreators";

const SignOutButton = ({ navigation, userSignOut }) => (
    <Icon.Button name="sign-out"
                 backgroundColor={'transparent'}
                 underlayColor={pinkLightColor}
                 onPress={() =>
                     signOut()
                         .then(() => {
                             userSignOut();
                             navigation.navigate('SignIn');
                         })
                         .catch(error => console.log('Error: ', error))
                 }
                 iconStyle={{marginRight: 0, paddingHorizontal: 10}}
                 size={25}
                 style={{marginRight: 10}}
    />
);

export default connect(
    null,
    dispatch => ({
        userSignOut: () => dispatch(logOutUser())
    })
)(SignOutButton);
