import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from "react-native";
import {pinkDarkColor} from "../config/styleConstants";

const Button = ({ onPress, text }) => {
    return (
        <TouchableOpacity style={styles.button} onPress={onPress}>
            <Text style={styles.buttonText}>{text}</Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    button: {
        backgroundColor: pinkDarkColor,
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderRadius: 10
    },
    buttonText: {
        color: 'white',
        fontSize: 18,
        textAlign: 'center'
    }
});

export default Button;