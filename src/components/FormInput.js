import React from 'react';
import {pinkDarkColor} from "../config/styleConstants";
import {StyleSheet, Text, TextInput, View} from "react-native";

const styles = StyleSheet.create({
    input: {
        color: 'white',
        fontSize: 18,
        width: 300,
    },
    formRow: {
        marginBottom: 20
    },
    errorText: {
        color: 'black'
    }
});

const inputProps = {
    underlineColorAndroid: 'white',
    placeholderTextColor: 'white',
    selectionColor: pinkDarkColor,
    style: styles.input
};

const FormInput = ({ error, inputRef, ...rest }) => {
    return (
        <View style={styles.formRow}>
            <TextInput {...{...inputProps, ...rest}} ref={inputRef}/>
            <Text style={styles.errorText}>{error}</Text>
        </View>
    );
};

export default FormInput;