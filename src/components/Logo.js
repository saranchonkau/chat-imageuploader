import React, {Component} from 'react';
import {View, Image} from 'react-native';

class Logo extends Component {
    render(){
        return (
            <View>
                <Image source={require('../images/logo.png')}/>
            </View>
        );
    }
}

export default Logo;