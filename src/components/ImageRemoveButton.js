import {pinkDarkColor, pinkLightColor} from "../config/styleConstants";
import {StyleSheet} from "react-native";
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {connect} from 'react-redux';
import {removeImage} from "../screens/ImagesScreen/ImageScreenActionCreators";

const RemoveButton = ({ item, deleteImage }) => (
    <Icon.Button name="trash-o"
                 backgroundColor={'transparent'}
                 underlayColor={pinkLightColor}
                 onPress={() => deleteImage(item)}
                 iconStyle={{marginRight: 0, paddingHorizontal: 10}}
                 size={45}
                 style={styles.removeButton}
                 borderRadius={0}
    />
);

const styles = StyleSheet.create({
    removeButton: {
        flexGrow: 1,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: pinkDarkColor
    }
});

export default connect(
    null,
    dispatch => ({
        deleteImage: image => dispatch(removeImage(image))
    })
)(RemoveButton);
