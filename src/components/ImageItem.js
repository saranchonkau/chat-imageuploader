import React, {Component} from 'react';
import {pinkDarkColor, pinkLightColor} from "../config/styleConstants";
import {ListItem} from "react-native-elements";
import Swipeout from 'react-native-swipeout';
import RemoveButton from './ImageRemoveButton';
import ProgressBar from 'react-native-progress/Bar';

class ImageItem extends Component {

    render() {
        const {item} = this.props;
        console.log('current progress: ', item.progress);
        return (
            <Swipeout autoClose={true}
                      right={[{ component: <RemoveButton item={item}/> }]}
                      backgroundColor={pinkLightColor}
            >
                <ListItem
                    roundAvatar
                    avatar={item}
                    title={item.name}
                    titleNumberOfLines={3}
                    subtitle={item.formattedSize}
                    hideChevron={true}
                    avatarStyle={{height: 100, width: 100}}
                    avatarContainerStyle={{height: 100, width: 100}}
                    titleStyle={{color: 'white', fontSize: 18}}
                    subtitleStyle={{fontSize: 16, color: 'black'}}
                    containerStyle={{borderBottomColor: pinkDarkColor}}
                />
                {
                    item.progress !== null &&
                    <ProgressBar progress={item.progress || 0} width={null} color={'#76ff03'}/>
                }
            </Swipeout>
        );
    }
}

export default ImageItem;