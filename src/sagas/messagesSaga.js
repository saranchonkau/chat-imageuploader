import { call, put, takeEvery } from 'redux-saga/effects';
import {convertParamsToUrl, http} from "../utils/http";
import {actionCreators, actionTypes} from "../screens/MessagesScreen/MessagesScreenReducer";

const requestMessages = ({ payload: { chatId, messageId = null, count = null }}) => {
    const params = {
        message_id: messageId,
        count: count
    };
    return http.get(`/chats/${chatId}/messages${convertParamsToUrl(params)}`);
};

// Fetch messages
function* fetchMessages(action) {
    try {
        const response = yield requestMessages(action);
        console.tron.log('Response');
        console.tron.log(response);
        yield put(actionCreators.fetchMessagesSuccess(response));
    } catch (error) {
        console.tron.log('Error');
        console.tron.log(error);
    }
}

// Fetch messages
function* fetchMoreMessages(action) {
    try {
        const response = yield requestMessages(action);
        console.tron.log('Response');
        console.tron.log(response);
        yield put(actionCreators.fetchMoreMessagesSuccess(response));
    } catch (error) {
        console.tron.log('Error');
        console.tron.log(error);
    }
}

export default function* rootSaga() {
    yield [
        takeEvery(actionTypes.FETCH_MESSAGES, fetchMessages),
        takeEvery(actionTypes.FETCH_MORE_MESSAGES, fetchMoreMessages)
    ]
}