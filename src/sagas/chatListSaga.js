import { call, put, takeEvery } from 'redux-saga/effects';
import {http} from "../utils/http";
import {actionCreators, actionTypes} from "../screens/ChatListScreen/ChatListScreenReducer";

//Fetch chat list
function* fetchChatList() {
    try {
        const response = yield http.get(`/chats`);
        console.tron.log('Response');
        console.tron.log(response);
        yield put(actionCreators.fetchChatListSuccess(response.list));
    } catch (error) {
        console.tron.log('Error');
        console.tron.log(error);
        yield put(actionCreators.fetchChatListFail());
    }
}

export default function* rootSaga() {
    yield takeEvery(actionTypes.FETCH_CHAT_LIST, fetchChatList);
}