import { call, put, takeEvery } from 'redux-saga/effects';
import {http} from "../utils/http";
import {actionCreators, actionTypes} from "../screens/MainMenuScreen/MainMenuScreenReducer";

// Fetch messages
function* fetchProfile() {
    try {
        const response = yield http.get('/profile');
        console.tron.log('Response');
        console.tron.log(response);
        yield put(actionCreators.fetchProfileSuccess(response.user));
    } catch (error) {
        console.tron.log('Error');
        console.tron.log(error);
    }
}

export default function* rootSaga() {
    yield takeEvery(actionTypes.FETCH_PROFILE, fetchProfile);
}