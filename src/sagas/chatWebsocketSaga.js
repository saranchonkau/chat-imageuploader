import io from 'socket.io-client';
import {eventChannel} from 'redux-saga';
import {fork, take, call, put, cancel, takeEvery, cancelled} from 'redux-saga/effects';
import {getAuthToken} from "../utils/http";
import Config from 'react-native-config';

/*#######   Socket events   #######*/

const socketEvents = {
    CONNECT: 'connect',
    CONNECT_ERROR: 'connect_error',
    CONNECT_TIMEOUT: 'connect_timeout',


    NEW_MESSAGE: 'msg:get',

    MESSAGE_SEND: 'msg:send',
    MESSAGE_SEND_SUCCESS: 'msg:send:ok',

    CHAT_READ: 'chat:read',
    CHAT_READ_SUCCESS: 'chat:read:ok'
};

/*#######   Action types   #######*/

export const actionTypes = {
    CONNECT_CHAT_SOCKET: 'CONNECT_CHAT_SOCKET',
    CLOSE_CHAT_SOCKET: 'CLOSE_CHAT_SOCKET',

    NEW_MESSAGE: 'NEW_MESSAGE',

    MESSAGE_SEND: 'MESSAGE_SEND',
    MESSAGE_SEND_SUCCESS: 'MESSAGE_SEND_SUCCESS',

    CHAT_READ: 'CHAT_READ',
    CHAT_READ_SUCCESS: 'CHAT_READ_SUCCESS'
};

/*#######   Action creators   #######*/

export const actionCreators = {
    connectChatSocket: () => ({ type: actionTypes.CONNECT_CHAT_SOCKET }),
    closeChatSocket: () => ({ type: actionTypes.CLOSE_CHAT_SOCKET }),

    newMessage: message => ({
        type: actionTypes.NEW_MESSAGE,
        message
    }),
    sendMessage: message => ({
        type: actionTypes.MESSAGE_SEND,
        data: message
    }),
    sendMessageSuccess: ({ guid, timestamp }) => ({
        type: actionTypes.MESSAGE_SEND_SUCCESS,
        response: { guid, timestamp }
    }),
    readChat: ({ room }) => ({
        type: actionTypes.CHAT_READ,
        data: { room }
    }),
    readChatSuccess: ({ user_id, room, timestamp }) => ({
        type: actionTypes.CHAT_READ_SUCCESS,
        response: { user_id, room, timestamp }
    })
};

/*#######   Create socket connection   #######*/

const connect = async () => {
    const token = await getAuthToken();
    return new Promise((resolve, reject) => {
        const socket = io(Config.CHAT_SOCKET_URL, {
            path: '/socket.io',
            query: {
                token: token,
                language: 'ru'
            }
        });
        socket.on(socketEvents.CONNECT, () => {
            resolve(socket);
        });
        socket.on(socketEvents.CONNECT_ERROR, () => {
            reject(new Error('Websocket connect error'));
        });
        socket.on(socketEvents.CONNECT_TIMEOUT, () => {
            reject(new Error('Websocket connect timeout'));
        });
    })
};

/*#######   Subscribe to socket events   #######*/

const subscribe = socket => {
    return eventChannel(emit => {
        socket.on(socketEvents.NEW_MESSAGE, ({ message }) => {
            emit(actionCreators.newMessage(message));
        });
        socket.on(socketEvents.MESSAGE_SEND_SUCCESS, ({ guid, timestamp }) => {
            emit(actionCreators.sendMessageSuccess({ guid, timestamp }));
        });
        socket.on(socketEvents.CHAT_READ_SUCCESS, ({ user_id, room, timestamp }) => {
            emit(actionCreators.readChatSuccess({ user_id, room, timestamp }));
        });
        return () => socket.close();
    });
};

function* read(socket) {
    const channel = yield call(subscribe, socket);
    try {
        while (true) {
            let action = yield take(channel);
            yield put(action);
        }
    } finally {
        if (yield cancelled()) {
            channel.close();
        }
    }
}

function* write(socket) {
    while (true) {
        const action = yield take([ actionTypes.MESSAGE_SEND, actionTypes.CHAT_READ ]);
        socket.emit(socketEvents[action.type], action.data);
    }
}

function* handleIO(socket) {
    yield fork(read, socket);
    yield fork(write, socket);
}

function* chatWebsocketFlow() {
    const socket = yield call(connect);
    const task = yield fork(handleIO, socket);
    yield take([actionTypes.CLOSE_CHAT_SOCKET, 'USER_LOGOUT']);
    yield cancel(task);
}

export default function* rootSaga() {
    yield takeEvery(actionTypes.CONNECT_CHAT_SOCKET, chatWebsocketFlow);
}