import chatList from './chatListSaga';
import messages from './messagesSaga';
import chatWebsocket from './chatWebsocketSaga';
import profileSaga from './profileSaga';

const sagas = {
    chatList,
    messages,
    profileSaga,
    chatWebsocket
};
export function registerWithMiddleware(middleware: { run: Function }) {
    for (let name in sagas) {
        middleware.run(sagas[name]);
    }
}