import {AsyncStorage} from 'react-native';

export const saveToken = token => AsyncStorage.setItem('userToken', token);

export const removeToken = () => AsyncStorage.removeItem('userToken');

export const getSize = nBytes => {
    let sOutput = nBytes + " bytes";
    // optional code for multiples approximation
    for (let aMultiples = ["KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"], nMultiple = 0, nApprox = nBytes / 1024; nApprox > 1; nApprox /= 1024, nMultiple++) {
        sOutput = nApprox.toFixed(3) + " " + aMultiples[nMultiple] + " (" + nBytes + " bytes)";
    }
    return sOutput;
};

export class Image {

    constructor({ data, fileName, fileSize, uri , type, path}){
        this.data = data;
        this.name = fileName;
        this.size = fileSize;
        this.uri = uri;
        this.type = type;
        this.path = path;
        this.formattedSize = getSize(fileSize);
        this.index = null;
        this.progress = null;
    }

    setIndex = index => this.index = index;

    setProgress = progress => this.progress = progress;
}