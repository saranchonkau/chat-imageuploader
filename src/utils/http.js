import 'whatwg-fetch';
import {AsyncStorage} from "react-native";
import Config from 'react-native-config';

const getHeaders = ({ token }) => {
    const headers = {
        'Content-Type': 'application/json',
    };
    if (token) {
        headers['Authorization'] = `JWT ${token}`;
    }
    return headers;
};

export const getAuthToken = () => AsyncStorage.getItem('userToken');

const handleResponse = response => {
    if (response.ok) {
        return response.json();
    } else {
        return response.json()
            .then(errors => {
                throw errors;
            })
    }
};

export const convertParamsToUrl = params => {
    if (params) {
        return '?' + Object.entries(params)
            .filter(([key, value]) => value !== undefined && value !== null)
            .map(([key, value]) => `${key}=${value}`).join('&');
    } else return '';
};

export const http = {
    configureUrl(path){
        return `${Config.HOST}${path}`;
    },
    simpleRequest(method, absoluteUrl, body){
        return getAuthToken()
            .then(token => {
                let options = {
                    method: method,
                    headers: getHeaders({ token })
                };
                if (body) {
                    options.body = JSON.stringify(body);
                }
                return fetch(new Request(absoluteUrl, options));
            })
            .then(handleResponse);
    },
    simpleGet(absoluteUrl, body){
        return this.simpleRequest('GET', absoluteUrl, body);
    },
    simplePost(absoluteUrl, body){
        return this.simpleRequest('POST', absoluteUrl, body);
    },
    simplePut(absoluteUrl, body){
        return this.simpleRequest('PUT', absoluteUrl, body);
    },
    simpleDelete(absoluteUrl, body){
        return this.simpleRequest('DELETE', absoluteUrl, body);
    },
    pathRequest(method, path, body){
        const url = this.configureUrl(path);
        return this.simpleRequest(method, url, body);
    },
    get(path){
        return this.pathRequest('GET', path);
    },
    post(path, body){
        return this.pathRequest('POST', path, body);
    },
    put(path, body){
        return this.pathRequest('PUT', path, body);
    },
    delete(path, body){
        return this.pathRequest('DELETE', path, body);
    }
};
