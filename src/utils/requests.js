import {getAuthToken, http} from "./http";
import RNFetchBlob from "react-native-fetch-blob";
import {removeToken} from "./utils";

export const registerDevice = () => {
    return getAuthToken()
        .then(token => {
            return http.post('/profile/device', {
                platform: 'ANDROID',
                token: token
            })
                .then(() => console.tron.log('Device was registered successfully'))
                .catch(error => console.tron.log(`Some error while device was registered. ${error}`));

        })
};

export const requestChatList = () => http.get('/chats');

export const sendImage = (image, updateProgress) => {
    return getAuthToken()
        .then(token => {
            return RNFetchBlob.fetch(
                'POST',
                'http://getmeet.dev.blak-it.com/api/media/image',
                {
                    Authorization: `JWT ${token}`,
                    // 'Content-Type' : 'multipart/form-data',
                },
                [{
                    name: 'file',
                    filename: image.name,
                    type: image.type,
                    data: RNFetchBlob.wrap(image.path)
                }]
            )
                .uploadProgress({ interval : 250 },(written, total) => {
                    console.log(`Uploaded: ${written / total}, Written: ${written}, Total: ${total}`);
                    updateProgress(image.index, written/total);
                })
                // listen to download progress event, every 10%
                .progress({ count : 10 }, (received, total) => {
                    console.log('progress', received / total)
                });
        })
};

export const sendImages = (images = [], updateProgress) => {
    if (images.length > 0) {
        const requestArray = images.map(image => {
            return sendImage(image, updateProgress)
                .then(response => {
                    console.log('Response: ', response);
                    return response.json()
                })
                .then(json => {
                    console.log('JSON : ', json);
                    return json;
                });
        });
        return Promise.all(requestArray);
    } else {
        return Promise.resolve([]);
    }
};

export const signOut = () => {
    console.tron.log('Sign out...');
    return getAuthToken()
        .then(token => ({
            platform: "ANDROID",
            token: token
        }))
        .then(body => {
            return http.post('/profile/logout', body)
                .then(removeToken);
        });
};

export const requestCode = phoneNumber => http.post('/auth/login', { phone: phoneNumber });

export const requestToken = (phoneNumber, codeSms) => http.post('/auth/code', { phone: phoneNumber, code: codeSms });