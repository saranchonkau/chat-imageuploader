import appReducer from "../reducers";
import Reactotron from 'reactotron-react-native';
import createSagaMiddleware from 'redux-saga';
import { applyMiddleware, compose } from 'redux'
import { registerWithMiddleware } from '../sagas';

export default () => {
    const sagaMiddleware = createSagaMiddleware({
        sagaMonitor: Reactotron.createSagaMonitor()
    });
    const middleware = applyMiddleware(sagaMiddleware);
    const store = Reactotron.createStore(appReducer, compose(middleware));
    registerWithMiddleware(sagaMiddleware);
    return store
}