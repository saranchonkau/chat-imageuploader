import { combineReducers } from 'redux';
import imagesReducer from '../screens/ImagesScreen/imagesScreenReducer';
import signInReducer from '../screens/SignInScreen/signInScreenReducer';
import messagesScreenReducer from '../screens/MessagesScreen/MessagesScreenReducer';
import chatListScreenReducer from '../screens/ChatListScreen/ChatListScreenReducer';
import mainMenuScreenReducer from '../screens/MainMenuScreen/MainMenuScreenReducer';

const rootReducer = combineReducers({
    images: imagesReducer,
    signIn: signInReducer,
    messages: messagesScreenReducer,
    chatList: chatListScreenReducer,
    profile: mainMenuScreenReducer
});

export default function appReducer(state, action){
    if (action.type === 'USER_LOGOUT') {
        // reducers are supposed to return the initial state
        // when they are called with undefined as the first argument,
        // no matter the action
        state = undefined;
    }

    return rootReducer(state, action);
};