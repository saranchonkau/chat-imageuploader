import './config/ReactotronConfig';
import React from 'react';
import Root from './navigation';
import { Provider } from 'react-redux';
import configureStore from './store';

console.disableYellowBox = true;
const store = configureStore();

const App = () => (
    <Provider store={store}>
        <Root/>
    </Provider>
);

export default App;